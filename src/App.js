import React from "react";
import { UrbanToken_address, UrbanToken_abi } from "./contract/UrbanToken";
import { useContract } from "web3-hooks";
import Dapp from "./Dapp";

export const UrbanTokenContext = React.createContext(null);

function App() {
  const UrbanToken = useContract(UrbanToken_address, UrbanToken_abi);

  return (
    <UrbanTokenContext.Provider value={UrbanToken}>
      <Dapp />
    </UrbanTokenContext.Provider>
  );
}

export default App;
