import { Box, Center, Text, Image, VStack } from "@chakra-ui/core";
import React from "react";
import { Link } from "react-router-dom";
import URBANLOGO from "../img/urban360logo (1).png";

function Footer() {
  return (
    <VStack py={10} w="100%" bg="#171923 ">
      <Box>
        <Link to="/">
          <Image
            maxWidth="150px"
            objectFit="cover"
            src={URBANLOGO}
            alt="logo URBAN 360"
          />
        </Link>
      </Box>

      <Text mx={1} color="white" fontSize="sm">
        {" "}
        &copy; 2021 | URBAN 360 - all right reserved.
      </Text>
      <Text color="white" fontSize="sm">
        By Lionel Djouhan
      </Text>
    </VStack>
  );
}

export default Footer;
