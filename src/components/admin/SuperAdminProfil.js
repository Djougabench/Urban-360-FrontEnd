import React, { useState, useContext } from "react";
import { UrbanTokenContext } from "../../App";

import {
  Input,
  SimpleGrid,
  VStack,
  FormControl,
  FormLabel,
  Stack,
  Heading,
  Button,
  Box,
  Text,
  Badge,
  HStack,
  ButtonGroup,
  IconButton,
  AddIcon,
} from "@chakra-ui/core";
import Nav from "../Nav";
import Footer from "../Footer";

function SuperAdminProfil() {
  const UrbanToken = useContext(UrbanTokenContext);

  const [inputAdressValueArtist, setInputAdressValueArtist] = useState("");
  const [inputAdressValueInvestor, setInputAdressValueInvestor] = useState("");
  const [inputValueArtistApproval, setInputValueArtistApproval] = useState("");
  const [inputValueInvestorApproval, setInputValueInvestorApproval] = useState(
    ""
  );

  const [getArtistApproval, setGetArtistApproval] = useState("");
  const [getInvestorApproval, setGetInvestorApproval] = useState("");

  const [getCounterInvestor, setgetCounterInvestor] = useState("");
  const [getCounterArtist, setgetCounterArtist] = useState("");

  const handleOnClickApproveArtist = async () => {
    console.log("ABOUT APPROVE ARTIST");
    const AppArt = await UrbanToken.approveArtist(inputAdressValueArtist);
  };

  const handleOnClickCounterInvestor = async () => {
    console.log("COUNTER INVESTOR");
    try {
      const res = await UrbanToken.counterInvestor();
      setgetCounterInvestor(res.toString());
    } catch (e) {}
  };

  const handleOnClickCounterArtist = async () => {
    console.log("COUNTER ARTIST");
    try {
      const res = await UrbanToken.counterArtist();
      setgetCounterArtist(res.toString());
    } catch (e) {}
  };

  const handleOnClickApproveInvestor = async () => {
    console.log("ABOUT APPROVE INVESTOR");
    const AppArt = await UrbanToken.approveInvest(inputAdressValueInvestor);
  };

  const handleOnClickgetArtistApproval = async () => {
    console.log("ABOUT THE ARTIST APPROVAL");
    try {
      const res = await UrbanToken.checkIfApprovedArtist(
        inputValueArtistApproval
      );
      console.log(res);
      setGetArtistApproval(res.toString());
    } catch (e) {}
  };

  const handleOnClickgetInvestorApproval = async () => {
    console.log("ABOUT THE INVESTOR APPROVAL");
    try {
      const res = await UrbanToken.checkIfApprovedInvest(
        inputValueInvestorApproval
      );
      console.log(res);
      setGetInvestorApproval(res.toString());
    } catch (e) {}
  };

  return (
    <>
      <Nav />

      <VStack fontFamily="Roboto Condensed, sans-serif " bg="#171923" pb={20}>
        <Heading
          color="white"
          mb={10}
          mt={150}
          fontFamily="Roboto Condensed, sans-serif "
        >
          Admin Dashboard
        </Heading>
        <SimpleGrid columns={[1, 1, 2]} spacing={10}>
          <Stack
            mx={5}
            mw="450px"
            minH="550px"
            p={10}
            boxShadow="xl"
            borderWidth="1px"
            borderRadius={5}
          >
            <HStack>
              <ButtonGroup
                mr={2}
                color="white"
                size="sm"
                isAttached
                variant="outline"
              >
                <Button onClick={handleOnClickCounterArtist} mr="-px">
                  count
                </Button>
              </ButtonGroup>
              <Text color="#FAF089">NUMBER OF ARTISTS REGISTRED :</Text>
              <Badge p={2} variant="outline" colorScheme="yellow">
                {getCounterArtist}
              </Badge>
            </HStack>
            <FormControl isRequired>
              <FormLabel htmlFor="ApproveA" color="white">
                {" "}
                Approve artists :
              </FormLabel>

              <Input
                id="ApproveA"
                mb={10}
                value={inputAdressValueArtist}
                onChange={(e) => {
                  setInputAdressValueArtist(e.currentTarget.value);
                }}
                color="#76E4F7"
                size="lg"
                type="text"
                variant="flushed"
                focusBorderColor="#76E4F7"
                aria-label="Approve Artist"
                placeholder="Artist ETH Adress"
              />

              <Button
                _hover={{
                  bg: "#76E4F7",
                  color: "white",
                }}
                colorScheme="cyan"
                width="full"
                my={2}
                onClick={handleOnClickApproveArtist}
              >
                Approve artists
              </Button>
            </FormControl>
            <Box>
              <Box mt={10}>
                <Text color="white">Check approval artists accounts :</Text>
                <Input
                  mb={10}
                  value={inputValueArtistApproval}
                  onChange={(e) => {
                    setInputValueArtistApproval(e.currentTarget.value);
                  }}
                  color="#76E4F7"
                  size="lg"
                  type="text"
                  variant="flushed"
                  focusBorderColor="#76E4F7"
                  aria-label="Approval"
                  placeholder="Artist ETH Adress"
                />
              </Box>
              <Button
                _hover={{
                  bg: "#76E4F7",
                  color: "white",
                }}
                colorScheme="cyan"
                width="full"
                my={2}
                onClick={handleOnClickgetArtistApproval}
              >
                Check Approval accounts
              </Button>

              <Stack p={5}>
                <Text color="white"> {getArtistApproval}</Text>
              </Stack>
            </Box>
          </Stack>

          <Stack
            mx={5}
            mw="450px"
            H="550px"
            p={10}
            boxShadow="xl"
            borderWidth="1px"
            borderRadius={5}
          >
            <HStack>
              <ButtonGroup
                mr={2}
                color="white"
                size="sm"
                isAttached
                variant="outline"
              >
                <Button onClick={handleOnClickCounterInvestor} mr="-px">
                  count
                </Button>
              </ButtonGroup>
              <Text color="#FAF089">NUMBER OF INVESTORS REGISTRED :</Text>
              <Badge p={2} variant="outline" colorScheme="yellow">
                {getCounterInvestor}
              </Badge>
            </HStack>
            <FormControl isRequired>
              <FormLabel htmlFor="ApprouveI" color="white">
                {" "}
                Approuve investors :
              </FormLabel>
              <Input
                mb={10}
                id="ApproveI"
                value={inputAdressValueInvestor}
                onChange={(e) => {
                  setInputAdressValueInvestor(e.currentTarget.value);
                }}
                color="#76E4F7"
                size="lg"
                type="text"
                variant="flushed"
                focusBorderColor="#76E4F7"
                aria-label="Approve Investor"
                placeholder="Investor ETH Adress"
              />
              <Button
                _hover={{
                  bg: "#76E4F7",
                  color: "white",
                }}
                colorScheme="cyan"
                width="full"
                my={2}
                onClick={handleOnClickApproveInvestor}
              >
                Approve investors
              </Button>
            </FormControl>
            <Box>
              <Box mt={10}>
                <Text color="white">Check approval investors accounts :</Text>
                <Input
                  mb={10}
                  id="ApprovalInvestor"
                  value={inputValueInvestorApproval}
                  onChange={(e) => {
                    setInputValueInvestorApproval(e.currentTarget.value);
                  }}
                  color="#76E4F7"
                  size="lg"
                  type="text"
                  variant="flushed"
                  focusBorderColor="#76E4F7"
                  aria-label="ApproveIApp"
                  placeholder="Investor ETH Adress"
                />
              </Box>
              <Button
                _hover={{
                  bg: "#76E4F7",
                  color: "white",
                }}
                colorScheme="cyan"
                width="full"
                my={2}
                onClick={handleOnClickgetInvestorApproval}
              >
                Check Approval accounts
              </Button>
              <Stack p={5}>
                <Text color="white">{getInvestorApproval}</Text>
              </Stack>
            </Box>
          </Stack>
        </SimpleGrid>
      </VStack>
      <Footer />
    </>
  );
}

export default SuperAdminProfil;
