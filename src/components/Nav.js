import React, { useContext } from "react";
import { Web3Context } from "web3-hooks";
import { UrbanTokenContext } from "../App";
import { Link } from "react-router-dom";
import { ChevronDownIcon } from "@chakra-ui/icons";
import "../App.css";

import {
  Box,
  Button,
  SimpleGrid,
  Flex,
  Spacer,
  Text,
  Image,
  useDisclosure,
  Drawer,
  DrawerOverlay,
  DrawerContent,
  DrawerBody,
  DrawerCloseButton,
  VStack,
  Stack,
  HStack,
  Input,
  InputGroup,
  InputLeftAddon,
} from "@chakra-ui/core";

import URBANLOGO from "../img/urban360logo (1).png";
import URBANLOGOSOLO from "../img/URBANFichier 3@4x.png";

function Nav() {
  const [web3State, login] = useContext(Web3Context);
  const UrbanToken = useContext(UrbanTokenContext);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const btnRef = React.useRef();

  return (
    <>
      <Flex
        boxShadow="2xl"
        opacity="90%"
        top={0}
        pos="fixed"
        w="100%"
        zIndex={2}
        bg="#171923"
        p={5}
      >
        <Box>
          <Link to="/">
            <Image
              maxWidth="200px"
              objectFit="cover"
              src={URBANLOGO}
              alt="logo URBAN 360"
            />
          </Link>
        </Box>
        <Spacer />
        {!UrbanToken && (
          <Button mx={3} colorScheme="cyan" onClick={login}>
            Connexion
          </Button>
        )}

        {UrbanToken !== null && web3State.chainId === 4 && (
          <>
            <Button
              name="button"
              variant="outline"
              ref={btnRef}
              onClick={onOpen}
              colorScheme="cyan"
            >
              MENU
              <ChevronDownIcon ml={2} />
            </Button>

            <Drawer
              isOpen={isOpen}
              placement="right"
              onClose={onClose}
              finalFocusRef={btnRef}
            >
              <DrawerOverlay>
                <DrawerContent bg="#171923">
                  <DrawerCloseButton color="white" />
                  <DrawerBody>
                    <VStack>
                      <Box py={20}>
                        <Link to="/">
                          <Image
                            maxWidth="100px"
                            objectFit="cover"
                            src={URBANLOGOSOLO}
                            alt="logo URBAN 360"
                          />
                        </Link>
                      </Box>
                      <Stack>
                        <InputGroup>
                          <InputLeftAddon children="Search" />
                          <Input
                            focusBorderColor="#76E4F7"
                            variant="outline"
                            placeholder="By artist ID "
                          />
                        </InputGroup>
                      </Stack>
                      <SimpleGrid
                        fontSize="xl"
                        spacing="5"
                        color="white"
                        pt={20}
                        pb={20}
                        px={10}
                      >
                        <Link to="/">
                          <Text
                            color=""
                            fontWeight="bold"
                            fontFamily="Roboto Condensed,sans -serif"
                          >
                            HOME
                          </Text>
                        </Link>
                        <Link to="/ArtistCard">
                          <Text
                            color="cyan"
                            fontWeight="bold"
                            fontFamily="Roboto Condensed,sans -serif"
                          >
                            DISCOVER ARTISTS
                          </Text>
                        </Link>
                        {/* <Link to="/">
                          <Text
                            fontWeight="bold"
                            fontFamily="Roboto Condensed,sans -serif"
                          >
                            INVESTORS
                          </Text>
        </Link>*/}

                        <Link to="/Form">
                          <Button
                            fontWeight="bold"
                            fontFamily="Roboto Condensed,sans -serif"
                            colorScheme="cyan"
                            variant="outline"
                          >
                            SIGN UP ARTIST
                          </Button>
                        </Link>
                        <Link to="InvestorForm/Form">
                          <Button
                            fontWeight="bold"
                            fontFamily="Roboto Condensed,sans -serif"
                            colorScheme="cyan"
                            variant="outline"
                          >
                            SIGN UP INVESTOR
                          </Button>
                        </Link>
                        <Link to="/SuperAdminProfil">
                          <Button
                            fontWeight="bold"
                            fontFamily="Roboto Condensed,sans -serif"
                            variant="outline"
                            colorScheme="red"
                          >
                            A D M I N
                          </Button>
                        </Link>
                      </SimpleGrid>
                    </VStack>

                    {/*<Box
                      fontFamily="Roboto Condensed,sans -serif"
                      fontSize={14}
                      columns={8}
                      space={1}
                      border="1px"
                      borderColor="gray.200"
                    >
                      <Text color="white" fontWeight="bold">
                        Web3 :
                      </Text>
                      {web3State.isWeb3 ? (
                        <Text color="green.500">injected</Text>
                      ) : (
                        <Text color="red.500">not found  </Text>
                      )}
                      <Text color="white">Wallet:</Text>
                      {web3State.isLogged ? (
                        <Text color="green.500">
                          {web3State.isMetamask} Connected {" "}
                        </Text>
                      ) : (
                        <Text color="red.500">
                          {!web3State.isMetamask} Disconnected 
                        </Text>
                      )}
                      <Text color="white">
                        Network name:
                        <Text color="green.500">{web3State.networkName}</Text>
                      </Text>
                      <Text color="white">
                        Network id:
                        <Text color="green.500"> {web3State.chainId}</Text>
                      </Text>
                      <Text color="white">
                        Account:
                        <Text color="green.500">{web3State.account}</Text>
                      </Text>
                      <Text color="white">Balance: {web3State.balance}</Text>
                      </Box>*/}
                  </DrawerBody>
                </DrawerContent>
              </DrawerOverlay>
            </Drawer>
          </>
        )}
        {UrbanToken !== null && web3State.chainId !== 4 && (
          <HStack>
            <Text
              mt={1}
              color="red.500"
              fontFamily="Roboto Condensed,sans -serif"
            >
              Connect to Rinkeby network !
            </Text>
          </HStack>
        )}
      </Flex>
    </>
  );
}

export default Nav;
