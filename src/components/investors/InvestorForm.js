import React, { useContext, useState } from "react";
import { UrbanTokenContext } from "../../App";
import { Web3Context } from "web3-hooks";
import { useHistory, Link } from "react-router-dom";
import { Box, Center, Heading, VStack, Button, Image } from "@chakra-ui/core";
import Nav from "../Nav";
import Footer from "../Footer";
import URBANLOGOSOLO from "../../img/URBANFichier 3@4x.png";

function InvestorForm() {
  const [web3State, login] = useContext(Web3Context);
  const UrbanToken = useContext(UrbanTokenContext);
  const history = useHistory();

  const [inputValueAddName, setInputValueAddName] = useState("");
  const [inputValueAddLastName, setInputValueAddLastName] = useState("");
  const [inputValueAddAge, setInputValueAddAge] = useState("");
  const [inputValueAddJobInvestor, setInputValueAddJobInvestor] = useState("0");
  const [inputValueAddEmail, setInputValueAddEmail] = useState("");

  const [inputValueAddAddress, setInputValueAddAddress] = useState("");

  const handleSubmitCreateInvestor = async () => {
    console.log("ABOUT CREATEINVESTOR");
    try {
      const REGISTERI = await UrbanToken.createInvestor(
        inputValueAddName,
        inputValueAddLastName,
        parseInt(inputValueAddAge),
        parseInt(inputValueAddJobInvestor),
        inputValueAddEmail,
        inputValueAddAddress
      );

      const cb = () => {
        history.push("/InvestorProfil");
      };

      const filter = UrbanToken.filters.InvestorCreated(web3State.account);
      UrbanToken.once(filter, cb);
    } catch (e) {
      console.log(`REGISTRATION ERROR: ${e}`);
    }
  };
  return (
    <>
      <Nav />
      {UrbanToken ? (
        <Box className="ConcertForm" bg="#171923" pt={20} pb={5}>
          <Center>
            <Heading
              color="white"
              mt={20}
              fontFamily="Roboto Condensed, sans-serif "
            >
              Create your Investor account
            </Heading>
          </Center>

          <div className=" signUp  text-white row col-lg-6 offset-lg-3 col-md-6 offset-md-3 col-sd-4 offset-sd-4 mt-5 mb-5 p-3 shadow-lg">
            <div className=" signUp ">
              <label htmlFor="FirstNameA" className="form-label mt-4">
                Name*
              </label>
              <input
                value={inputValueAddName}
                onChange={(e) => {
                  setInputValueAddName(e.currentTarget.value);
                }}
                className="form-control mb-2"
                id="FirstNameA"
                type="text"
                placeholder="Name"
                name="FirstNameA"
                aria-label="FirstNameA"
              />
              <label htmlFor="LastnameA" className="form-label">
                Lastname*
              </label>
              <input
                value={inputValueAddLastName}
                onChange={(e) => {
                  setInputValueAddLastName(e.currentTarget.value);
                }}
                className="form-control mb-2"
                id="LastnameA"
                type="text"
                placeholder="Last name"
                name="LastnameA"
                aria-label="LastnameA"
              />
              <label htmlFor="AgeA" className="form-label ">
                Age*
              </label>
              <input
                value={inputValueAddAge}
                onChange={(e) => {
                  setInputValueAddAge(e.currentTarget.value);
                }}
                className="form-control mb-2"
                id="AgeA "
                type="number"
                placeholder="Age"
                name="AgeA"
                aria-label="AgeA"
              />

              <label htmlFor="JobInvestor" className="form-label">
                Your Job*
              </label>
              <select
                value={inputValueAddJobInvestor}
                onChange={(e) => {
                  setInputValueAddJobInvestor(e.currentTarget.value);
                }}
                multiple={false}
                id="JobInvestor"
                type="number"
                name="JobInvestor"
                className="form-select form-select-md mb-2"
                aria-label="JobInvestor"
              >
                <option value="0">Lawyer</option>
                <option value="1">Photographer</option>
                <option value="2">Studio Owner</option>
                <option value="3">Music Producer</option>
                <option value="4">video Director</option>
                <option value="5">Communication</option>
              </select>
              <label htmlFor="Email" className="form-label">
                Email address*
              </label>
              <input
                value={inputValueAddEmail}
                onChange={(e) => {
                  setInputValueAddEmail(e.currentTarget.value);
                }}
                className="form-control mb-2"
                id="EmailA"
                type="EmailA"
                placeholder="Email "
                name="EmailA"
                aria-label="EmailA"
              />
              <label htmlFor="EthAddress" className="form-label">
                Eth Address
              </label>
              <input
                value={inputValueAddAddress}
                onChange={(e) => {
                  setInputValueAddAddress(e.currentTarget.value);
                }}
                className="form-control mb-2"
                id="EthAddress"
                type="text"
                placeholder="Eth Adress"
                name="EthAddress"
                aria-label="EthAddress"
                // ref={{ required: true }}
              />

              <div className=" mx-auto d-grid mt-4 mb-3 ">
                <button
                  onClick={handleSubmitCreateInvestor}
                  id="buttonCreateA"
                  className="btn btn-info "
                >
                  Register
                </button>
              </div>
            </div>
          </div>
        </Box>
      ) : (
        !web3State.isLogged && (
          <VStack pt={200} pb={50} bg="#171923">
            <Box>
              <Link to="/">
                <Image
                  maxWidth="400px"
                  objectFit="cover"
                  src={URBANLOGOSOLO}
                  alt="logo URBAN 360"
                />
              </Link>
            </Box>
            <Box pt={100}>
              <Button colorScheme="cyan" onClick={login}>
                Connexion to Metamask
              </Button>
            </Box>
          </VStack>
        )
      )}
      <Footer />
    </>
  );
}
export default InvestorForm;
