import React, { useState, useContext, useEffect } from "react";
import {
  Text,
  Center,
  Heading,
  Stack,
  Input,
  SimpleGrid,
  Box,
  Badge,
  Spacer,
} from "@chakra-ui/core";

import { UrbanTokenContext } from "../../App";
import { Web3Context } from "web3-hooks";
import Nav from "../Nav";
import { JobNeededtoName } from "../artists/JobNeeded";
import Footer from "../Footer";
import handleOnClickApproveArtist from "../admin/SuperAdminProfil";

function InvestorProfil() {
  const UrbanToken = useContext(UrbanTokenContext);
  const [web3State, login] = useContext(Web3Context);

  const [firstName, setName] = useState("");
  const [lastname, setLastName] = useState("");
  const [age, setAge] = useState("");
  const [email, setEmail] = useState("");
  const [investorJob, setInvestorJob] = useState("");
  const [etheraddress, setetheraddress] = useState("");

  useEffect(() => {
    (async () => {
      if (UrbanToken !== null) {
        const getInvestorInfo = await UrbanToken.getInvestorInfo();
        console.log(getInvestorInfo);
        setName(getInvestorInfo[0]);
        setLastName(getInvestorInfo[1]);
        setAge(getInvestorInfo[2]);
        setEmail(getInvestorInfo[3]);
        setInvestorJob(getInvestorInfo[4]);
        setetheraddress(getInvestorInfo[5]);
      }
    })();
  }, [UrbanToken, web3State.account]);
  return (
    <>
      <Nav />
      <Box pt={20} bg="#171923">
        <Center>
          <Heading
            color="white"
            fontFamily="Roboto Condensed, sans-serif"
            m={5}
            pt={50}
          >
            Investors Details
          </Heading>
        </Center>

        <Center>
          <Stack
            mx={5}
            minw="300px"
            minH="400px"
            p={20}
            boxShadow="2xl"
            borderRadius={5}
            mb={10}
          >
            {handleOnClickApproveArtist && (
              <Stack direction="row" pb={5}>
                <Badge variant="outline" colorScheme="green">
                  approved
                </Badge>
                <Spacer />
              </Stack>
            )}
            <SimpleGrid spacingX="30px" spacingY="10px">
              <Text color="white" fontWeight="bold">
                First Name :
              </Text>

              <Input
                color="#76E4F7"
                size="lg"
                type="text"
                variant="flushed"
                focusBorderColor="#76E4F7"
                aria-label="ApproveA"
                value={firstName}
              />

              <Text color="white" fontWeight="bold">
                Lastname :
              </Text>
              <Input
                color="#76E4F7"
                size="lg"
                type="text"
                variant="flushed"
                focusBorderColor="#76E4F7"
                aria-label="ApproveA"
                value={lastname}
              />
              <Text color="white" fontWeight="bold">
                {" "}
                Age:
              </Text>
              <Input
                color="#76E4F7"
                size="lg"
                type="text"
                variant="flushed"
                focusBorderColor="#76E4F7"
                aria-label="ApproveA"
                value={age}
              />
              <Text color="white" fontWeight="bold">
                Email:
              </Text>
              <Input
                color="#76E4F7"
                size="lg"
                type="text"
                variant="flushed"
                focusBorderColor="#76E4F7"
                aria-label="ApproveA"
                value={email}
              />
              <Text color="white" fontWeight="bold">
                Your Job:
              </Text>
              <Input
                color="#76E4F7"
                size="lg"
                type="text"
                variant="flushed"
                focusBorderColor="#76E4F7"
                aria-label="ApproveA"
                value={JobNeededtoName(investorJob)}
              />
              <Text color="white" fontWeight="bold">
                Ether address:{" "}
              </Text>
              <Input
                color="#76E4F7"
                size="lg"
                type="text"
                variant="flushed"
                focusBorderColor="#76E4F7"
                aria-label="ApproveA"
                value={etheraddress}
              />
            </SimpleGrid>
          </Stack>
        </Center>
      </Box>
      <Footer />
    </>
  );
}

export default InvestorProfil;
