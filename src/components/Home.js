import React from "react";
import { Link } from "react-router-dom";
import "../App.css";
import Nav from "./Nav";
import Footer from "./Footer";
import {
  Button,
  Text,
  VStack,
  Box,
  HStack,
  Container,
  Center,
} from "@chakra-ui/core";
import IMAGE1 from "../img/lhi.png";
import IMAGE2 from "../img/tizzy.png";

function Home() {
  return (
    <>
      <Nav />

      <VStack px={5} pt={200} pb={100} className="Travis">
        <h5 color="#171923 " className="exist display-3 text-info ">
          ARTISTS AND LABELS
        </h5>

        <div className=" row col-lg-6  text-white mx-2">
          <h5 className=" exist c display-6">
            All you need to develop your artist career is here .
          </h5>
          <Text className="exist ">
            Urban 360 gives artists the best tool to develop their music.here,
            your lawyer,your photographer or your beatmaker could become your
            producer. Urban 360 gives the oportunity to all inde label with p
            small budget to make it.
          </Text>
          <Link to="/Form">
            <Box mt={5} mb={150}>
              <p className="btn btn-info  ">Sign up</p>
            </Box>
          </Link>
        </div>

        <Center>
          <Container className="  ">
            <h5 className="text-info  exist display-3 text-in">
              INVEST IN NEW TALENTS.
            </h5>

            <Container className="exist text-white ">
              Urban 360 gives artists the best tool to develop their music.here,
              your lawyer,your photographer or your beatmaker could become your
              producer. Urban 360 gives the oportunity to all inde label with p
              small budget to make it.
              <HStack>
                <Link to="/InvestorForm">
                  <p className="btn btn-info my-3">Sign up</p>
                </Link>
                <Link to="/ArtistCard">
                  <Button
                    mx={2}
                    fontWeight="bold"
                    variant="outline"
                    colorScheme="cyan"
                  >
                    Discover artists
                  </Button>
                </Link>
              </HStack>
            </Container>
          </Container>
        </Center>
      </VStack>

      <Footer />
    </>
  );
}
export default Home;
