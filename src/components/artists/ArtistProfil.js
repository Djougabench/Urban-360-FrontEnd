import React, { useState, useContext, useEffect } from "react";
import {
  Text,
  Input,
  SimpleGrid,
  Center,
  Heading,
  Stack,
  Badge,
  Box,
  Spacer,
} from "@chakra-ui/core";
import { UrbanTokenContext } from "../../App";
import { Web3Context } from "web3-hooks";
import Nav from "../Nav";
import { JobNeededtoName } from "./JobNeeded";
import Footer from "../Footer";
import handleOnClickApproveArtist from "../admin/SuperAdminProfil";

function ArtistProfil() {
  const UrbanToken = useContext(UrbanTokenContext);
  const [web3State] = useContext(Web3Context);

  const [firstName, setName] = useState("");
  const [lastname, setLastName] = useState("");
  const [age, setAge] = useState("");
  const [email, setEmail] = useState("");
  const [jobneeded, setJobNeeded] = useState("");

  useEffect(() => {
    (async () => {
      if (UrbanToken !== null) {
        const getArtistInfo = await UrbanToken.getArtistInfo();
        console.log(getArtistInfo);
        setName(getArtistInfo[0]);
        setLastName(getArtistInfo[1]);
        setAge(getArtistInfo[2]);
        setEmail(getArtistInfo[3]);
        setJobNeeded(getArtistInfo[4]);
      }
    })();
  }, [UrbanToken, web3State.account]);
  return (
    <>
      <Nav />
      <Box pt={20} bg="#171923">
        <Center>
          <Heading
            color="white"
            fontFamily="Roboto Condensed, sans-serif"
            m={5}
            pt={50}
          >
            Artist Details
          </Heading>
        </Center>

        <Center>
          <Stack
            mx={5}
            minw="300px"
            minH="400px"
            p={20}
            boxShadow="2xl"
            borderRadius={5}
            mb={10}
          >
            {handleOnClickApproveArtist && (
              <Stack direction="row" pb={5}>
                <Badge variant="outline" colorScheme="green">
                  approved
                </Badge>
                <Spacer />
              </Stack>
            )}
            <SimpleGrid spacingX="30px" spacingY="10px">
              <Text color="white" fontWeight="bold">
                First Name :
              </Text>

              <Input
                color="#76E4F7"
                size="lg"
                type="text"
                variant="flushed"
                focusBorderColor="#76E4F7"
                aria-label="ApproveA"
                value={firstName}
              />

              <Text color="white" fontWeight="bold">
                Lastname :
              </Text>
              <Input
                color="#76E4F7"
                size="lg"
                type="text"
                variant="flushed"
                focusBorderColor="#76E4F7"
                aria-label="ApproveA"
                value={lastname}
              />
              <Text color="white" fontWeight="bold">
                {" "}
                Age:
              </Text>
              <Input
                color="#76E4F7"
                size="lg"
                type="text"
                variant="flushed"
                focusBorderColor="#76E4F7"
                aria-label="ApproveA"
                value={age}
              />
              <Text color="white" fontWeight="bold">
                Email:
              </Text>
              <Input
                color="#76E4F7"
                size="lg"
                type="text"
                variant="flushed"
                focusBorderColor="#76E4F7"
                aria-label="ApproveA"
                value={email}
              />
              <Text color="white" fontWeight="bold">
                Invest Needed:
              </Text>
              <Input
                color="#76E4F7"
                size="lg"
                type="text"
                variant="flushed"
                focusBorderColor="#76E4F7"
                aria-label="ApproveA"
                value={JobNeededtoName(jobneeded)}
              />
            </SimpleGrid>
          </Stack>
        </Center>
      </Box>
      <Footer />
    </>
  );
}

export default ArtistProfil;
