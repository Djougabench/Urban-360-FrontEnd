export const JobNeededtoName = (theJobNeeded) => {
  switch (theJobNeeded) {
    case 0:
      return "Lawyer";
    case 1:
      return "Photographer";
    case 2:
      return "Studio Owner";
    case 3:
      return "Music Producer";
    case 4:
      return "video Director";
    case 5:
      return "Communication";
    default:
      return "unknown";
  }
};
