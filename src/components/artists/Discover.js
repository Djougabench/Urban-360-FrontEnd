import React, { useState, useContext } from "react";
import { UrbanTokenContext } from "../../App";
import {
  HStack,
  AspectRatio,
  VStack,
  Box,
  Heading,
  Text,
  Button,
  Badge,
  Input,
} from "@chakra-ui/core";

const Discover = (props) => {
  const UrbanToken = useContext(UrbanTokenContext);
  const [
    inputDiscoverAdressValueInvestor,
    setInputDiscoverAdressValueInvestor,
  ] = useState("");
  const [
    inputDiscoverAdressValueArtist,
    setInputDiscoverAdressValueArtist,
  ] = useState("");

  const [getArtistDiscoverTokenID, setgetArtistDiscoverTokenID] = useState("");
  //const [getInvestLinkedTokenID, setgetInvestLinkedTokenID] = useState("");

  const handleOnClickInvestOn = async () => {
    console.log("ABOUT INVEST ON ARTIST");
    try {
      const res = await UrbanToken.ubnTokenArtist(
        inputDiscoverAdressValueInvestor,
        inputDiscoverAdressValueArtist
      );

      console.log(res);
      setgetArtistDiscoverTokenID(res.toString());
    } catch (e) {}
  };

  return (
    <>
      <Box bg="#171923">
        <VStack>
          <Box p={10}>
            <Badge my={2} variant="subtle" colorScheme="green">
              {props.musicStyle}
            </Badge>
            <AspectRatio maW="300px" ratio={16 / 9}>
              <iframe
                title={props.videoTitle}
                src={props.videoLink}
                frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              />
            </AspectRatio>
            <HStack>
              <Heading mr={2} fontSize="3xl" color="white">
                {props.artistName}
              </Heading>
              <Text fontSize="3xl" color="#76E4F7">
                {props.artistSong}
              </Text>
            </HStack>

            <VStack>
              <Text color="WHite"></Text>
            </VStack>
            <Box minW="300px">
              <Input
                value={inputDiscoverAdressValueArtist}
                onChange={(e) => {
                  setInputDiscoverAdressValueArtist(e.currentTarget.value);
                }}
                mb={10}
                id="DiscoverA"
                color="#76E4F7"
                size="lg"
                type="text"
                variant="flushed"
                focusBorderColor="#76E4F7"
                aria-label="DiscoverA"
                placeholder="Artist ETH Adress"
              />
              <Input
                value={inputDiscoverAdressValueInvestor}
                onChange={(e) => {
                  setInputDiscoverAdressValueInvestor(e.currentTarget.value);
                }}
                mb={10}
                id="DiscoverI"
                color="#76E4F7"
                size="lg"
                type="text"
                variant="flushed"
                focusBorderColor="#76E4F7"
                aria-label="DiscoverI"
                placeholder="Investor ETH Adress"
              />
              <Button
                onClick={handleOnClickInvestOn}
                width="full"
                colorScheme="cyan"
              >
                {props.investButton}
              </Button>

              <Box p={5}>
                <Text color="white"> TOKEN ID :{getArtistDiscoverTokenID}</Text>
              </Box>
            </Box>
          </Box>
        </VStack>
      </Box>
    </>
  );
};

export default Discover;
