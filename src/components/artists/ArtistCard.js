import React from "react";
import Discover from "./Discover";
import Nav from "../Nav";
import Footer from "../Footer";
import { SimpleGrid, Center, Heading, Box } from "@chakra-ui/core";

const ArtistCards = () => {
  return (
    <>
      <Nav />
      <Box pb={20} bg="#171923">
        <Center mb={10} mt={200}>
          <Heading color="white">Discover</Heading>
        </Center>
        <SimpleGrid>
          <Box>
            <Discover
              artistName="LHIROYD"
              musicStyle="AFROBEAT-RAP"
              videoTitle="chance"
              videoLink="https://www.youtube.com/embed/2GQfGGiFhFk"
              artistSong="CHANCE"
              investButton="Invest on Lhiroyd"
            />
          </Box>
          <Box /*mt={10}*/>
            <Discover
              artistName="TIZZY MILLER"
              musicStyle="RAP"
              videoTitle="Président"
              videoLink="https://www.youtube.com/embed/6fqbSyv4cmo"
              artistSong="PRESIDENT"
              investButton="Invest on Tizzy Miller"
            />
          </Box>
        </SimpleGrid>
      </Box>
      <Footer />
    </>
  );
};

export default ArtistCards;
