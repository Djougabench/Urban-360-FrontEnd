import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Form from "./components/artists/Form";
import Home from "./components/Home";
import ArtistProfil from "./components/artists/ArtistProfil";
import InvestorForm from "./components/investors/InvestorForm";
import InvestorProfil from "./components/investors/InvestorProfil";
import SuperAdminProfil from "./components/admin/SuperAdminProfil";
import ArtistCard from "./components/artists/ArtistCard";

function Dapp() {
  return (
    <Router>
      <Switch>
        <Route path="/Form" component={Form} />
        <Route path="/" exact component={Home} />
        <Route path="/ArtistProfil" component={ArtistProfil} />
        <Route path="/InvestorForm" component={InvestorForm} />
        <Route path="/InvestorProfil" component={InvestorProfil} />
        <Route path="/SuperAdminProfil" component={SuperAdminProfil} />
        <Route path="/ArtistCard" component={ArtistCard} />
      </Switch>
    </Router>
  );
}

export default Dapp;
