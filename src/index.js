import React from "react";
import ReactDOM from "react-dom";
import reportWebVitals from "./reportWebVitals";
import App from "./App";
import "bootstrap/dist/css/bootstrap.css";
import { ChakraProvider } from "@chakra-ui/core";
import "focus-visible/dist/focus-visible";
import { Web3Provider } from "web3-hooks";
import { extendTheme, GlobalStyle, CSSReset, ITheme } from "@chakra-ui/core";

const config = (theme: ITheme) => ({
  dark: {
    bg: theme.colors.gray[800],
  },
});

const theme = extendTheme({
  style: {
    global: {
      "html, body": {
        fontFamily: "Roboto Condensed, sans-serif, Prompt, sans-serif",
      },
    },
  },
});

ReactDOM.render(
  <React.StrictMode>
    <ChakraProvider theme={theme} resetCSS={true}>
      <CSSReset config={config} />
      <GlobalStyle style={theme} />
      <Web3Provider>
        <App />
      </Web3Provider>
    </ChakraProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
